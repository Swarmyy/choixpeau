from math import sqrt
from collections import Counter
import csv
from operator import truediv

profile_1 = {'Courage': '9', 'Ambition': '2', 'Intelligence': '8', 'Good': '9'}
profile_2 = {'Courage': '6', 'Ambition': '7', 'Intelligence': '9', 'Good': '7'}
profile_3 = {'Courage': '3', 'Ambition': '8', 'Intelligence': '6', 'Good': '3'}
profile_4 = {'Courage': '2', 'Ambition': '3', 'Intelligence': '7', 'Good': '8'}
profile_5 = {'Courage': '3', 'Ambition': '4', 'Intelligence': '8', 'Good': '8'}

profiles = [profile_1, profile_2, profile_3, profile_4, profile_5]

NAME = 'Name'
LENGTH = 'length'
HOUSE = 'House'


def open_hp(file: str):
    with open(file, mode='r', encoding='utf-8') as f:
        reader = csv.DictReader(f, delimiter=';')
        return [{key: value.replace(
            '\xa0', ' ') for key, value in element.items()} for element in reader]


def attr_to_int(*dictionnaries: dict):
    for d in dictionnaries:
        for k in d:
            try:
                d[k] = int(d[k])
            except ValueError:
                pass


def manh_length(point: dict[str, int], neighbour: dict[str, int]):
    return sqrt(sum([(point[stat] - neighbour[stat])**2 for stat in profile_1.keys()]))


def eucl_length(point: dict[str, int], neighbour: dict[str, int]):
    return sum([abs(point[stat]-neighbour[stat]) for stat in profile_1.keys()])


def closests(point: dict[str, int], neighbours: list[dict[str, int]], k=5):
    lengths = [{NAME: neigh[NAME], HOUSE: neigh[HOUSE], LENGTH: manh_length(
        point, neigh)} for neigh in neighbours]
    return sorted(lengths, key=lambda key: key[LENGTH])[:k]


def choose_house_counter(point: dict[str, int], neighbours: list[dict[str, int]], k=5, nearest=1):
    # House : number of occurences
    return [t[0] for t in Counter([d[HOUSE] for d in closests(point, neighbours, k)]).most_common(nearest)]


def choose_house(point: dict[str, int], neighbours: list[dict[str, int]], k=5):
    # [('Ravenclaw', 2), ('Hufflepuff', 2), ('Slytherin', 1)] will make the first choice
    counter = {}
    for house in [d[HOUSE] for d in closests(point, neighbours)]:
        if house not in counter:
            counter[house] = 1
        else:
            counter[house] += 1
    return sorted(counter, key=lambda key: counter[key], reverse=True)[0]


def add_house(attrs: list[dict], chars_full: list[dict]):
    for char in attrs:
        name = char[NAME]
        for is_c in chars_full:
            if is_c[NAME] == name:
                char[HOUSE] = is_c[HOUSE]
                break


def ref():
    chars_attr = open_hp("Caracteristiques_des_persos.csv")
    chars = open_hp("Characters.csv")

    attr_to_int(*chars_attr)

    add_house(chars_attr, chars)
    return chars_attr


if __name__ == '__main__':
    attr_to_int(*profiles)
    for p in profiles:
        print(choose_house_counter(p, ref(), 5, 1))

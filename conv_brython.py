from main import choose_house_counter, profiles, ref, attr_to_int


def slug(point: dict[str, int], neighbours: list[dict[str, int]], k=5, nearest = 1):
    house = choose_house_counter(point, neighbours, k, nearest)
    if nearest == 1:
        return house[0]
    return ' '.join(house)

if __name__ == '__main__':
    attr_to_int(*profiles)
    for p in profiles:
        print(slug(p, ref(), 5 , 1))

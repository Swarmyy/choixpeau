from browser import document, bind, console, html
from main import choose_house_counter, closests, ref, profile_1, NAME, HOUSE
from conv_brython import slug

QUESTION = {
    "question1":
        {
            "rep1": (1, -1, 1, 1),
            "rep2": (1, 2, -3, 4),
            "rep3": (0, 1, 1, -1),
            "rep4": (-4, 3, -1, 2)
        },
        "question2":
        {
            "rep2_1": (-1, 1, 1, -1),
            "rep2_2": (1, 2, -3, 4),
            "rep2_3": (0, 1, 1, 1),
            "rep2_4": (4, -3, 1, -2)
        }
}

# IDs
TITLE = "title"
FORM = "form"
NEXT = "next"
FINAL = "final"
QDIV = "qq"
QI_ = "q"
QL_ = "l"
ANS = "answer"


def to_list(d):
    return [(k, v) for k, v in d.items()]


# ANSWERS
VALUES = []

# Properly change questions
CONV_Q = to_list(QUESTION)  # converted dict into list for better usage
COUNTER = 0  # which question are we at
LENGTH = len(CONV_Q)


def hide(id):
    document[id].style.display = "none"


def show(id):
    document[id].style.display = "inline"


@bind(f"#{NEXT}", "click")
def next(e):
    global COUNTER
    cq = CONV_Q[COUNTER]
    createQuestion(cq[0], cq[1])
    save()

    COUNTER += 1
    check_final()


@bind(f"#{FINAL}", "click")
def final(e):
    save()
    stats = [sum([v[i] for v in VALUES]) for i in range(len(VALUES[0]))]
    profile = {stat_name: stats[i] for i, stat_name in enumerate(profile_1)}
    result = closests(profile, ref())
    console.log("preslug")
    house = slug(profile, ref)
    console.log("postslug")
    console.log(house)
    nears = ";".join([f"{p[NAME]}, maison: {p[HOUSE]}" for p in result])
    document[ANS] <= html.SPAN(f"Votre maison: {'house'}. Personnages proches : {nears}")


def save():
    for nq in range(4):
        if document[f"q{nq+1}"].checked:
            global VALUES
            checked = to_list(CONV_Q[COUNTER-1][1])[nq]  # rep: values
            VALUES.append(checked[1])
            break


def createQuestion(title, q):
    document[TITLE].textContent = title
    i = 0
    for ans, val in q.items():
        document[f"{QI_}{i+1}{QL_}"].textContent = ans
        i += 1


def check_final():
    if COUNTER == LENGTH:
        hide(NEXT)
        show(FINAL)


next(None)
